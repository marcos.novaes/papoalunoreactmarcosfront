import React, { useState, useEffect } from 'react'
import axios from 'axios'
import FormList from './formlist'
import List from './list'
import './lists.css'

const Lists = () => {

    const [lists, setLists] = useState([])

    const getLists = async () => {
        try {
            const result = await axios.get('https://papoalunobackendlista.herokuapp.com/lists')
            setLists(result.data)
        } catch (error) {
            setLists([])
            console.log(error.response.data.erro)
        }
    }

    const removeList = async (id) => {
        try {
            await axios.delete(`https://papoalunobackendlista.herokuapp.com/lists/${id}`)
            getLists()
        } catch (error) {
            alert(error.message)
        }

    }

    const createList = async (body) => {
        try {
            const result = await axios.post('https://papoalunobackendlista.herokuapp.com/lists/new', body)
            setLists([
                ...lists,
                result.data
            ])
        } catch (error) {
            alert(error.message)
        }

    }

    const editList = async (body, id) => {
        try {
            await axios.put(`https://papoalunobackendlista.herokuapp.com/lists/${id}`, body)
            alert('lista alterada com sucesso')
            getLists()

        } catch (error) {
            alert(error.message)
        }

    }

    const montaListas = () => {
        return (
            lists.map((list, i) => (<List key={i} index={i} editarLista={editList} removerLista={removeList} lista={list}></List>))
        )
    }

    useEffect(() => {
        getLists()
    })


    return (
        <div className="page">
            <FormList criarLista={createList}></FormList>
            <div className="listas">
                {lists.length > 0 ? montaListas() : <h2 className="semListas">Não tem listas disponiveis</h2>}
            </div>
        </div>
    )


}

export default Lists