import React, { useState } from 'react'
import FormItem from './formItem'
import './lists.css'

const FormList = ({ criarLista }) => {

    const [form, setForm] = useState({})
    const [items, setItems] = useState([])

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
        return
    }

    const addItem = (value) => {
        setItems([
            ...items,
            value
        ])
    }

    const montaBody = () => {
        const body = {
            ...form
        }
        body["items"] = items
        return body
    }

    const submitForm = async () => {

        try {
            const body = montaBody()
            criarLista(body)
            setForm({})
            setItems([])
        } catch (error) {
            alert(error.message)
        }
    }

    const mapeiaUrgencia = (value) => {
        return value === "true" ? "urgente" : "não urgente"
    }

    return (
        <div className="caixaFormLista">
            <div className="formLista">
                <h2>Crie uma nova Lista</h2>
                <div className="form-group">
                    <label htmlFor="lista">Nome:</label>
                    <input onChange={(e) => handleChange(e)} value={form.nome || ""} name="nome" id="inputNome" />
                </div>
                <FormItem incluirTarefa={addItem} ></FormItem>
                <ul>
                    {items.map((item, i) => {
                        return <li key={i}>{item.descricao + " | " + mapeiaUrgencia(item.urgente)}</li>
                    })}
                </ul>
                <button onClick={() => submitForm()} >Criar Lista!</button>
            </div>
        </div>
    )


}

export default FormList